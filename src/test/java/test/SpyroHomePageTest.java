package test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import spyro.pages.contactus.ContactUsForm;
import spyro.pages.home.HomePage;
import spyro.routes.Route;

@ContextConfiguration(locations = {"classpath:spring-test-config.xml"})
public class SpyroHomePageTest extends AbstractTestNGSpringContextTests {

    @Autowired
    private WebDriver webDriver;

    @Autowired
    private WebDriverWait wait;

    @Autowired
    private Route<HomePage> homePageRoute;

    @Test(groups = "spyro")
    public void should_appear_email_validation_alert() {
        HomePage homePage = homePageRoute.open();
        ContactUsForm submittedForm = homePage
                .getMenuBar()
                .findContactUsPageLink()
                .get()
                .getContactUsForm()
                .fillName("Dupa Dupa")
                .fillEmail("xxxx12")
                .fillPhoneNumber("123456789")
                .submit();
        wait.until( wd -> submittedForm.findEmailValidationSpan() );
        Assert.assertTrue( submittedForm.findEmailValidationSpan().isPresent() );
        Assert.assertFalse( submittedForm.findPhoneNumberValidationSpan().isPresent() );

    }

    @Test(groups = "spyro")
    public void should_appear_phone_validation_alert() {
        HomePage homePage = homePageRoute.open();
        ContactUsForm submittedForm = homePage
                .getMenuBar()
                .findContactUsPageLink()
                .get()
                .getContactUsForm()
                .fillName("Dupa Dupa")
                .fillEmail("xxxx12@wp.pl")
                .fillPhoneNumber("sdsada")
                .submit();
        wait.until( wd -> submittedForm.findPhoneNumberValidationSpan() );
        Assert.assertTrue( submittedForm.findPhoneNumberValidationSpan().isPresent() );
        Assert.assertFalse( submittedForm.findEmailValidationSpan().isPresent() );

    }

    @AfterTest
    public void after() {
        webDriver.quit();
    }

    @AfterClass
    public void afterClass(){
        webDriver.close();
    }

}
