package spyro;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FirefoxConfiguration {

    @Bean
    public WebDriver webDriver(){
        return new FirefoxDriver();
    }

    @Bean
    public WebDriverWait webDriverWait(WebDriver webDriver){

        return new WebDriverWait(webDriver, 120);
    }


}
