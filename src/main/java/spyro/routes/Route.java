package spyro.routes;

public interface Route<T> {

    T open();
    T getPageComponent();

}
