package spyro.routes;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import lombok.Getter;
import spyro.pages.contactus.ContactUsPage;
import spyro.pages.home.HomePage;
import spyro.routes.Route;

@Component
@Getter
public class Routes {

    @Autowired
    private Route<HomePage> homePageRoute;

    @Autowired
    private Route<ContactUsPage> contactUsPageRoute;
}
