package spyro.routes;

import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import spyro.pages.contactus.ContactUsPage;
import spyro.pages.home.HomePage;
import spyro.routes.Route;

@Configuration
public class RoutesConfiguration {

    @Bean
    public Route<HomePage> homePageRoute(WebDriver webDriver, HomePage homePage){
        return new Route<HomePage>() {
            @Override
            public HomePage open() {
                webDriver.get("http://www.spyro-soft.com");
                return homePage;
            }

            @Override
            public HomePage getPageComponent() {
                return homePage;
            }
        };
    }


    @Bean
    public Route<ContactUsPage> contactUsPageRoute(WebDriver webDriver, ContactUsPage contactUsPage){
        return new Route<ContactUsPage>() {
            @Override
            public ContactUsPage open() {
                webDriver.get("https://www.spyro-soft.com/contact-us");
                return contactUsPage;
            }

            @Override
            public ContactUsPage getPageComponent() {
                return contactUsPage;
            }
        };
    }

}
