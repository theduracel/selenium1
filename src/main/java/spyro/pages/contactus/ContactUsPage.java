package spyro.pages.contactus;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import lombok.Getter;
import spyro.pages.MenuBar;

@Component
@Getter
public class ContactUsPage {

    @Autowired
    private MenuBar menuBar;

    @Autowired
    private ContactUsForm contactUsForm;
}
