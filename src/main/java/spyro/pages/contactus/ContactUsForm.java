package spyro.pages.contactus;

import java.util.Optional;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spyro.components.Input;
import spyro.components.Span;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;

@Component
public class ContactUsForm {

    @Autowired
    private WebDriver webDriver;

    public ContactUsForm fillName(String name){
        findNameInput().sendKeys(name);
        return this;
    }

    public ContactUsForm fillEmail(String name){
        findEmailInput().sendKeys(name);
        return this;
    }

    public ContactUsForm fillPhoneNumber(String phone){
        findPhoneNumberInput().sendKeys(phone);
        return this;
    }

    public ContactUsForm submit(){
        findSendButton().submit();
        return this;
    }

    public Input findNameInput(){
        WebElement yourNameElement = webDriver.findElement(By.xpath("//input[@name='your-name']"));
        return new Input(yourNameElement);
    }

    public Input findEmailInput(){
        WebElement yourEmailElement = webDriver.findElement(By.xpath("//input[@name='your-email']"));
        return new Input(yourEmailElement);
    }

    public Input findPhoneNumberInput(){
        WebElement telWebElement = webDriver.findElement(By.xpath("//input[@name='tel']"));
        return new Input(telWebElement);
    }

    public Input findSendButton(){
        WebElement telWebElement = webDriver.findElement(By.xpath("//input[@type='submit']"));
        return new Input(telWebElement);
    }


    public Optional<Span> findEmailValidationSpan(){
        try{
            WebDriverWait wait = new WebDriverWait(webDriver, 3);
            WebElement alertSpan = wait.until( elementToBeClickable(By.xpath("//span[contains(@class, 'your-email')]/span[@role='alert']")) );
            return Optional.of(new Span(alertSpan));
        } catch (TimeoutException e){
            return Optional.empty();
        }
    }

    public Optional<Span> findPhoneNumberValidationSpan(){
        try{
            WebDriverWait wait = new WebDriverWait(webDriver, 3);
            WebElement alertSpan = wait.until( elementToBeClickable(By.xpath("//span[contains(@class, 'tel')]/span[@role='alert']")) );
            return Optional.of(new Span(alertSpan));
        } catch (TimeoutException e){
            return Optional.empty();
        }
    }


}
