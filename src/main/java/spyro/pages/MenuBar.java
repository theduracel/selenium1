package spyro.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import spyro.routes.Routes;
import spyro.components.Link;
import spyro.pages.contactus.ContactUsPage;
import spyro.pages.home.HomePage;

@Component
public class MenuBar {

    @Autowired
    private WebDriver webDriver;

    @Autowired
    private Routes routes;

    public Link<HomePage> findGotoHomePageLink(){
        return new Link<>(webDriver.findElement(By.id("menu-item-19")), () -> routes.getHomePageRoute().getPageComponent());
    }

    public Link<ContactUsPage> findContactUsPageLink(){
        return new Link<>(webDriver.findElement(By.id("menu-item-105")), () -> routes.getContactUsPageRoute().getPageComponent());
    }
}
