package spyro.components;

import java.util.function.Supplier;
import org.openqa.selenium.WebElement;

public class InputSubmit<T> extends Input {

    private Supplier<T> inputPageSupplier;

    public InputSubmit(WebElement webElement, Supplier<T> pageComponentSupplier) {
        super(webElement);
    }

    public T hit(){
        submit();
        return inputPageSupplier.get();
    }
}
