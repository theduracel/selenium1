package spyro.components;

import java.util.function.Supplier;
import org.openqa.selenium.WebElement;
import lombok.Getter;
import lombok.experimental.Delegate;
import spyro.routes.Route;

public class Link<T> implements WebElement {

    @Delegate private WebElement webElement;
    @Getter private Supplier<T> pageComponentSupplier;

    public Link(WebElement webElement, Supplier<T> pageComponentSupplier) {
        this.webElement = webElement;
        this.pageComponentSupplier = pageComponentSupplier;
    }

    public T get(){
        webElement.click();
        return pageComponentSupplier.get();
    }

}
