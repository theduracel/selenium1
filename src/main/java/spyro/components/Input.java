package spyro.components;

import org.openqa.selenium.WebElement;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Delegate;

@AllArgsConstructor
public class Input implements WebElement {

    @Delegate @NonNull private WebElement webElement;


}
