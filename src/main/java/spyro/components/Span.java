package spyro.components;

import org.openqa.selenium.WebElement;
import lombok.AllArgsConstructor;
import lombok.experimental.Delegate;

@AllArgsConstructor
public class Span implements WebElement {

    @Delegate private WebElement webElement;
}
